import json
import datetime

class CatalogManager:

    def __init__(self,fileName):
        f=open(fileName,'r')
        self.obj=json.load(f)
        f.close()
        
        self.fileName=fileName

        self.prevUpdate=self.obj['lastUpdate']

        measureType=[]
        houses=self.obj['houses']
        for i in range(len(houses)):
            devs=self.obj['houses'][i]['devicesList']
            for j in range(len(devs)):
                for singleMeasure in devs[j]['measureType']:
                    if singleMeasure not in measureType:
                        measureType.append(singleMeasure)

        self.availableMeasures=measureType


    def searchDeviceByID(self,ID):
        """print all the information about the device for the given <deviceID>"""

        houses=self.obj['houses']
        for i in range(len(houses)):
            devs=self.obj['houses'][i]['devicesList']
            for j in range(len(devs)):
                if devs[j]['deviceID']==ID:
                    info=devs[j]
                    print(json.dumps(devs[j], indent=4))
                    break
            else:
                continue
            break
        else:
            print('\nNo devices for the given ID!')
        return devs[j]

    def searchDevicesByHouseID(self,houseID):
        """print all the information about the devices for the given <houseID>"""

        ck=True
        houses=self.obj['houses']
        for i in range(len(houses)):
            if houses[i]['houseID']==houseID:
                devs=self.obj['houses'][i]['devicesList']
                for j in range(len(devs)):
                    info=devs[j]
                    print('\n\tDevice: %s\n' %info['deviceName'])
                    print(json.dumps(devs[j], indent=4))
                    print('\n')
                break
        #Commento a caso
        # commento per branch
        else:
            print('\nNo devices for the given house ID!')
            ck=False
        if ck:
            return devs


    def searchUserByUserID(self,userID):
        """print all the information of a user given its <userID>"""

        ck=True
        users=self.obj['usersList']
        for i in range(len(users)):
            if users[i]['userID']==userID:
                print(json.dumps(users[i], indent=4))
                break
        else:
            print('\nNo users for the given ID!')
            ck=False
        if ck:
            return users[i]


    def searchDevicesByMeasureType(self, type):
        """print all the information about any device that provides such measure <type>"""
        
        ck=True
        if type not in self.availableMeasures:
            print(f'\n{type} measure not available!')
            ck=False
        else:
            found=[]
            houses=self.obj['houses']
            for i in range(len(houses)):
                devs=self.obj['houses'][i]['devicesList']
                for j in range(len(devs)):
                    if type in devs[j]['measureType']:
                        info=devs[j]
                        print('\n',json.dumps(devs[j], indent=4))
                        found+=[info]
        if ck:
            return found

    def insertDevice(self,userID,houseID,devDict):
        """insert a new device it that is not already present on the list (the ID is checked) given the device details,
        the user ID and the house ID. Otherwise update the information about the existing device with the new parameters.
        Every time that this operation is performed the "last_update" field needs to be updated with the current date and 
        time in the format "yyyy-mm-dd hh:mm". The structure of the parameters of the file must follow the one of the
        ones that are already present"""

        devs=[]
        for house in self.obj['houses']:
            if house['houseID']==houseID and house['userID']==userID:
                devs=house['devicesList']
                break
        else:     
            IDs=[]
            for singleDev in devs:
                IDs+=[singleDev['deviceID']]

            try:
                devs[IDs.index(devDict['deviceID'])].update(devDict)
                devs[IDs.index(devDict['deviceID'])]['lastUpdate']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            except ValueError:      # new device
                devs.append(devDict)
                devs[-1]['lastUpdate']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            
            self.obj['lastUpdate']=datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
            
        print('\nWrong combination of house ID and user ID!')


    def printAll(self):
        """print the full catalog"""
        #print(json.dumps(self.obj,indent=4))
        return json.dumps(self.obj,indent=4)        #secondo me questa cosa non ha senso


    def exit(self):
        """save the catalog (if changed) in the same JSON file provided as input."""
        if self.obj['lastUpdate']!=self.prevUpdate:
            f=open(self.fileName,'w')
            json.dump(self.fileName,f,indent=4)
            f.close()
            print('\n',json.dumps(self.obj,indent=4),'\nChanges saved!')
        

############################################################################################
############################################################################################


if __name__=='__main__':
    cat=CatalogManager('catalog.json')
    while True:
        
        user_input=input("\nThe available functions are:\n\
\t1: searchDeviceByID\n\
\t2: searchDevicesByHouseID\n\
\t3: searchUserByUserID\n\
\t4: searchDevicesByMeasureType\n\
\t5: insertDevice\n\
\t6: printAll\n\
\t7: exit\n\
\tq: Quit session\n\n\
Choose one: ")

        if user_input=='1':
            ID=int(input('\n\tsearchDeviceByID -->\n\nInsert device ID: '))
            print('\nJSON file:\n',cat.searchDeviceByID(ID))

        elif user_input=='2':
            houseID=int(input('\n\tsearchDeviceByHouseID -->\n\nInsert house ID: '))
            print('\nJSON file:\n',cat.searchDevicesByHouseID(houseID))

        elif user_input=='3':
            userID=int(input('\n\tsearchUserByUserID -->\n\nInsert user ID: '))
            print('\nJSON file:\n',cat.searchUserByUserID(userID))
        
        elif user_input=='4':
            print('\n\tsearchDevicesByMeasureType -->\n\nAvailable measure types:\n')
            for meas in cat.availableMeasures:
                print(f'\t- {meas}')
                type=input('\nInsert measure type: ')
                print('\nJSON file:\n',cat.searchDevicesByMeasureType(type))
            
        elif user_input=='5':
            print('\n\tinsertDevice --> SORRY NON-TRIABLE NOW')
            
        elif user_input=='6':
            print('\n\tprintAll -->\n')
            print(cat.printAll())

        elif user_input=='7':
            print('\n\tprintAll -->\n')
            cat.exit()
        
        elif user_input=='q':
                break
        
        else:
                print('\nCommand not recognized')
    
    print('\nGoodbye!\n\n')