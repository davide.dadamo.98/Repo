from Exercise_0 import *
if __name__=='__main__':
    while True:
            user_input=input("\nChoose the exercise:\n\
            0: SquareManager object\n\
            1: 2D Point object\n\
            2: Line object\n\
            3: Deck and cards\n\
            q: Quit session")
                
            if int(user_input)==0:                                                                #ex00
                print('\n\n\t#ex00: SquareManager object\n')
                    
                sm=SquareManager(3)
                print(f"The area of the square with side {sm.side} = {sm.area()}")
                print(f"The perimeter of the square with side {sm.side} = {sm.perimeter()}")
                print(f"The diagonal of the square with side {sm.side} = {sm.diagonal():.3f}")

            elif int(user_input)==1:                                                                #ex01
                print('\n\n\t#ex01: 2D Point object\n')
                    
                # create 2 points and calculate distance among them
                a=Point(7,1)
                b=Point(1,1)
                print(a.distance(b))

                # move a point according to a vector (x,y)
                a.move(2,2)
                print(a.x,a.y)

                #ex2
            
            elif int(user_input)==2:                                                                #ex02
                
                print('\n\n\t#ex02: Line object\n')

                # 1 Simple creation
                l1=Line(m=3,q=2)
                print(l1)

                #2 Create line from 2 points
                a=Point(0,1)
                b=Point(2,2)
                l2=Line()
                l2.line_from_points(a,b)
                print(l2)

                #3 Function for distance from point and intersection with another line
                l=Line(m=1,q=0)
                a=Point(1,5)
                print(l.distance(a))
                m=Line(-1,0)
                i=l.intersection(m)
                print(i)

            elif int(user_input)==3:                                                                #ex03
                
                print('\n\n\t#ex03: Deck and cards\n')
                


            elif user_input=='q':                                                                   #quit
                break
            
            else:                                                                                   #error
                print('\nCommand not recognized')
        
    print('\nGoodbye!\n\n')
