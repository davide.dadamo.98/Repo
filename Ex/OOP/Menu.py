from Exercise_0 import *
if __name__=='__main__':
    while True:
        user_input=input("\nThe available command are:\n\
        d: Distance between 2 points\n\
        m: Move a point according to a vector\n\
        q: Quit session\n")

        if user_input=='d':
            x,y=map(int, input("\nInsert x and y coordinates of first point (P1):\n").split())
            p1=Point(x,y)

            x,y=map(int, input("Insert x and y coordinates of second point (P2):\n").split())
            p2=Point(x,y)
            
            d=p1.distance(p2)
            print(f'\nDistance of P2 from P1 is {d:.4}')
        elif user_input=='m':
            x,y=map(int, input("\nInsert x and y coordinates of the point:\n\t").split())
            p=Point(x,y)
            vx,vy=map(int, input("Insert x and y coordinates of the vector:\n\t").split())

            p.move(vx,vy)

            print(f'\nNew point coordinates: x={p.x} y={p.y}')
        elif user_input=='q':
            break
        else:
            print('\nCommand not recognized')
    print('\nGoodbye!\n\n')

        
            