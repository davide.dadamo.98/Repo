class SquareManager:
    "This is a square manager, you need just to enter the sidelength"
    def __init__(self,l):
        self.side=l
    
    def area(self):
        return self.side**2

    def perimeter(self):
        return self.side*4
    
    def diagonal(self):
        return self.side*2**0.5

import math as m
class Point:
    def __init__(self,x,y):
        self.x=x
        self.y=y
    
    def distance(self,other):
        dx=(other.x-self.x)**2
        dy=(other.y-self.y)**2
        d=m.sqrt(dx+dy)
        return d
    
    def move(self,dx,dy):
        self.x+=dx
        self.y+=dy

class Line:
    def __init__(self,m='none',q='none'):
        self.m=m
        self.q=q

    def line_from_points(self,a,b):
        self.m=(b.y-a.y)/(b.x-a.x)
        self.q=a.y-a.x*self.m

    def distance(self,a):
        N=abs(self.m*a.x-a.y+self.q)
        D=m.sqrt(self.m+1)
        return N/D
    
    def intersection(self,l):
        x=(l.q-self.q)/(self.m-l.m)
        y=self.m*x+self.q
        p=Point(x,y)
        return p

import random
class Deck:
    def __init__(self):
        self.deck=range(0,52)
        self.drawn=[]
        self.discardPile=[]
        self.cards={
            'id' : [self.deck[0:13],self.deck[13:26],self.deck[26:39],self.deck[39:52]],
            'hearts' : ['A','2','3','4','5','6','7','8','9','10','J','Q','K'],
            'diamonds' : ['A','2','3','4','5','6','7','8','9','10','J','Q','K'],
            'clubs' : ['A','2','3','4','5','6','7','8','9','10','J','Q','K'],
            'spades' : ['A','2','3','4','5','6','7','8','9','10','J','Q','K'],
        }

    def deal(self,n):
        suits=list(self.cards.keys())
            # scarta le carte pescate precedentemente
        for i in len(self.drawn):
            self.discardPile.append(self.drawn.pop())
        i=0
        drawn=[]
            # pesca n nuove carte
        while i<n:
            self.drawn.append(self.deck.pop())
            count=0
            card_check=self.drawn[-1]
            while card_check-12>0:
                card_check=-12
                count=+1
            suit=self.cards[suits[count]]
            num=self.cards[suit][card_check]
            drawn[i]=f'{num} of {suit}'
            print(drawn[i])
            i+=1
        return drawn
            
    def tell_card(self,id):
            pass

    def shuffle(self):
        self.deck=random.shuffle(self.deck)

        