#ex00
if __name__=="__main__":
    #This is the main
    ex=int(input("Exercise n°: "))

    if ex==1:
        #ex01
        print('\n\t#ex01: Printing strings\n')
        print('è la prima volta che scrivo in python')
        print("prova caratteri:\nfunziona l'accapo?")
        print('çé°*§ù+àèòàì^?!"£$%&/()€')
    
    if ex==2:
        #ex02
        print('\n\n\t#ex02: Printing variables\n')
        
        name="Davide"
        age=23
        birthday=29
        birthmonth=4
        birthyear=1998

        #   print with '%' placeholder
        print("\tWith '%' placeholder")
        print("My name is %s and I'm %d years old, I was born on the %d/%02d/%s\n" %(name,age,birthday,birthmonth,str(birthyear)[-2:]))

        #   print with f'_' tecnique
        print("\tWith f'_' tecnique")
        print(f"My name is {name} and I'm {age} years old, I was born on the {birthday}/{birthmonth:02}/{str(birthyear)[-2:]}")

    if ex==3:
        #ex03
        print('\n\n\t#ex03: Math operators\n')

        #  +
        print(f'3+4={3+4}')
        #  -
        print(f'3-4={3-4}')
        #  /
        print(f'3/4={3/4}')
        #  //
        print(f'3//4={3//4}')
        #  *
        print(f'3*4={3*4}')
        #  %
        print(f'3%4={3%4}')
        #  <
        print(f'3<4={3<4}')
        #  >
        print(f'3>4={3>4}')
        #  <=
        print(f'3<=4={3<=4}')
        #  >=
        print(f'3>=4={3>=4}')
        #  ==
        print(f'3==4={3==4}')
        #  !=
        print(f'3!=4={3!=4}')
    
    if ex==4:
        #ex04
        print('\n\n\tex04: Getting user inputs\n')

        name=input('Please, write your name and press enter:\n\t')
        age=input('Now write your age and press enter:\n\t')
        height=input('Now write your height (in m) and press enter:\n\t')

        print(f"\nSo you're {name}, you're {age} years old and tall {height} m ... ??")

    if ex==5:
        #ex05
        print('\n\n\tex05: File managing\n')

        f=open('original.txt')
        org=f.read()
        f.close()

        print(org)

        header='The content of the original file is:\n\n'
        f=open('copy.txt','w')
        f.write(header+org+f'\n\n{3*4}  {name}') #posso scrivere su file come se fosse un print sul terminal
        f.close()

    if ex==6:
        #ex06
        print('\n\n\tex06: If-Elif-Else blocks\n')

        n=int(input('Please, write a number and press enter:\n\t'))
        
        if n%2==0 and n%3==0:
            print(f'{n} is a multiple of both 2 and 3!')
        elif n%2==0 and n%3!=0:
            print(f'{n} is a multiple of 2')
        elif n%2!=0 and n%3==0:
            print(f'{n} is a multiple of 3')
        else:
            print(f'{n} is not a multiple of 2 or 3')
   
    if ex==7:
        #ex07
        print('\n\n\tex07: For-While blocks\n')

        print('Please, write a list of numbers separated by one blank space:\n')
        s=input('\t')
        nums=s.split()
        fls=[float(x) for x in nums]
        
        #media
        avg=0
        for n in fls:
            avg+=n
        avg=avg/len(fls)

        #max
        maxx=fls[0]
        for n in fls[1:len(fls)]:
            if n>maxx:
                maxx=n

        #min
        minn=fls[0]
        for n in fls[1:len(fls)]:
            if n<minn:
                minn=n
        
        print(f'Average value:\t{avg}')
        print(f'Maximum value:\t{maxx}')
        print(f'Minimum value:\t{minn}')

    if ex==8 or ex==9:
        #ex08-09
        print('\n\n\tex08-09: Dictionaries and JSON files\n')

        personal_data={
            'projectName':'',
            'company':'',
            'deviceList':{
                    'deviceID':'',
                    'deviceName':'',
                    'deviceType':''
            }
        }

        print('Original information:')
        for a in personal_data.items():
            print(a)

        print('\nPlease, insert requested information:')
        for k1,it1 in personal_data.items():
            if isinstance(it1,dict):
                for k2,it2 in it1.items():
                    it1[k2]=input(f'{k2}: ')
            else:
                personal_data[k1]=input(f'{k1}: ')
        
        print('\nInformation updated!')
        for a in personal_data.items():
            print(a)

        import json
        f=open('personal_data.json','w')
        json.dump(personal_data,f)
        f.close()
        print('\nDictionary saved!')

    if ex==10:
        #ex10
        print('\n\n\tex10: Functions\n')

        def useless_function(arg):
            arg_2=arg+arg
            return arg_2

        y=useless_function('IoT')
        print(y)
        z=useless_function([1,2,3])
        print(z)

        def somma(x,y):
            return x+y
        def sottrazione(x,y):
            return x-y
        def moltiplicazione(x,y):
            return x*y
        def divisione(x,y):
            return x/y

        s=input('\nChe operazione vuoi effettuare?\n[somma,sottrazione,moltiplicazione,divisione]\n\t')
        print('\nDimmi i due numeri:')
        a=float(input('\t'))
        b=float(input('\t'))

        c=eval(s)(a,b)

        print(f'Il risultato è: {c:.4}')    